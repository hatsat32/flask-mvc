from flask import Flask
from .controllers import blue_prints, auth, home

app = Flask(__name__)

for bp in blue_prints:
	app.register_blueprint(bp)
