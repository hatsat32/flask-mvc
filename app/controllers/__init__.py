from flask import Blueprint
from . import home, auth

blue_prints = (
	home.home_bp, auth.auth_bp
)
